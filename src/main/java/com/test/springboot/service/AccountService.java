package com.test.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.springboot.mapper.Account;
import com.test.springboot.mapper.AccountMapper;

@Service
public class AccountService {

	@Autowired
	AccountMapper mapper;
	
	public List<Account> findAll() {
		
		return mapper.findAll();
	}

}
