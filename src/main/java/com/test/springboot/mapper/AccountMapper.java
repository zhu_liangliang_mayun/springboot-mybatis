package com.test.springboot.mapper;

import java.util.List;

public interface AccountMapper {
	void add(Account account);
	List<Account> findAll();
}
