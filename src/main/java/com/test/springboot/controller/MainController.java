package com.test.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.test.springboot.mapper.Account;
import com.test.springboot.service.AccountService;


@Controller
public class MainController {

	
	@Autowired
	AccountService accSrv;
	
	
	@RequestMapping("/list")
	@ResponseBody
	public Object list() {
		
		List<Account>  accounts = accSrv.findAll();
		
		return accounts;
	}
	
}
